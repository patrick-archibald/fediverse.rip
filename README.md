# fediverse.rip

Content for https://fediverse.rip website. 

[Fediverse](https://en.wikipedia.org/wiki/Fediverse) instances come and go. We should maintain a list of the dead. 

Keeping this site as sparse as possible. One text file to edit: index.txt

Two columns separated by a tab are domain name and approximate date died in yyyy/mm/dd format.


